import { Idioma } from "../models/Idioma.js";

export async function getIdiomes(token) {
  const fetchIdiomes = await fetch(
    "https://server247.cfgs.esliceu.net:24743/piano/google/translate/languages",
    {
      method: "POST",
      headers: {
        Authorization: token,
      },
    }
  );

  const idiomes = await fetchIdiomes.json();
  return idiomes.map((i) => new Idioma(i.code, i.name));
}

export async function traduccioCatala(langFrom, text, token) {
  const fetchTraduccio = await fetch(
    "https://server247.cfgs.esliceu.net:24743/piano/google/translate",
    {
      method: "POST",
      headers: {
        Authorization: token,
      },
      body: JSON.stringify({
        languageFrom: langFrom,
        languageTo: "ca",
        text: text,
      }),
    }
  );

  const traduccio = await fetchTraduccio.text();
  return traduccio;
}

export async function sendVideo(blob, token) {
  const formData = new FormData();

  formData.append("arxiu", blob);

  const fetchVideo = await fetch(
    "https://server247.cfgs.esliceu.net:24743/piano/google/transcribe",
    {
      method: "POST",
      headers: {
        Authorization: token,
      },
      body: formData,
    }
  );
  const video = await fetchVideo.json();
  return video;
}
