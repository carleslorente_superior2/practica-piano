import { Partitura } from "../models/Partitura.js";
import { Nota } from "../models/Nota.js";

export async function getPartitures(token) {
  const fetchPartitures = await fetch(
    "http://server247.cfgs.esliceu.net/piano/score/list",
    {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: token,
      },
    }
  );

  const partitures = await fetchPartitures.json();
  const notes = [];

  partitures.forEach((p) => {
    p.notes.sort((a, b) =>
      a.ordre > b.ordre ? 1 : b.ordre > a.ordre ? -1 : 0
    );
    notes.push(
      p.notes.map((n) => new Nota(n.idnota, n.nom, n.alteracio, n.ordre))
    );
  });

  return partitures.map(
    (p, i) =>
      new Partitura(
        p.idpartitura,
        p.titol,
        p.idiomaoriginal,
        p.idiomatraduccio,
        p.lletraoriginal,
        p.lletratraduccio,
        notes[i]
      )
  );
}

export async function getPartituraById(id, token) {
  const fetchPartitura = await fetch(
    "http://server247.cfgs.esliceu.net/piano/score/get",
    {
      method: "POST",
      headers: {
        Authorization: token,
      },
      body: JSON.stringify({
        id: id,
      }),
    }
  );

  const partitura = await fetchPartitura.json();
  return new Partitura(
    partitura.idpartitura,
    partitura.titol,
    partitura.idiomaoriginal,
    partitura.idiomatraduccio,
    partitura.lletraoriginal,
    partitura.lletratraduccio,
    partitura.notes.sort((a, b) =>
      a.ordre > b.ordre ? 1 : b.ordre > a.ordre ? -1 : 0
    )
  );
}

export async function addPartitura(
  id,
  nom,
  partituraOrg,
  partituraTrad,
  idiomaOrg,
  idiomaTrad,
  notes,
  token
) {
  const fetchPartitura = await fetch(
    "http://server247.cfgs.esliceu.net/piano/score/save",
    {
      method: "POST",
      headers: {
        Authorization: token,
      },
      body: JSON.stringify({
        score: {
          idpartitura: id,
          name: nom,
          partituraoriginal: partituraOrg,
          partituratraduccio: partituraTrad,
          idiomaoriginal: idiomaOrg,
          idiomatraduccio: idiomaTrad,
          notes: notes,
        },
      }),
    }
  );

  return fetchPartitura.json();
}

export async function deletePartitura(id, token) {
  const fetchPartitura = await fetch(
    "http://server247.cfgs.esliceu.net/piano/score/delete",
    {
      method: "POST",
      headers: {
        Authorization: token,
      },
      body: JSON.stringify({
        id: id,
      }),
    }
  );

  return fetchPartitura.json();
}

export function addCerca(nota, cerca) {
  if (nota.nom == "DO_AGUT") {
    cerca.push("DO'");
  } else if (nota.alteracio == "SOSTINGUT") {
    cerca.push(`${nota.nom}#`);
  } else {
    cerca.push(nota.nom);
  }
  return cerca;
}

export function cercador(cerca, partitures) {
  let notes = "";

  cerca.forEach((c) => {
    notes += c;
  });

  let canço = "";
  let cançons = [];

  partitures.forEach((p) => {
    p.notes.forEach((n) => {
      if (n.nom == "DO_AGUT") {
        canço += "DO'";
      } else {
        canço += n.nom;
      }
      if (n.alteracio == "SOSTINGUT") {
        canço += "#";
      }
    });
    cançons.push(canço);
    canço = "";
  });

  let resultat = [];

  cançons.forEach((c, i) => {
    if (c.includes(notes)) {
      resultat.push(partitures[i]);
    }
  });

  return resultat;
}
