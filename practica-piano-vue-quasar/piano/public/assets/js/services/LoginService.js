export async function login(usuari, password) {
  const fetchLogin = await fetch(
    "http://server247.cfgs.esliceu.net/piano/auth/login",
    {
      method: "POST",
      body: JSON.stringify({
        usuari: usuari,
        password: password,
      }),
    }
  );

  if (fetchLogin.status == 200) {
    return fetchLogin.text();
  } else {
    return fetchLogin.json();
  }
}
