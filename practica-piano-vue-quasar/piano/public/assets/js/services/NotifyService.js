export function showNotification(resultat) {
  if (window.Notification && Notification.permission !== "denied") {
    Notification.requestPermission(function (status) {
      const n = new Notification(`${resultat.notifyType}`, {
        body: `${resultat.notifyMessage}`,
        icon: "../assets/img/nota-musical.ico",
      });
      setTimeout(function () {
        n.close();
      }, 5000);
    });
  }
}
