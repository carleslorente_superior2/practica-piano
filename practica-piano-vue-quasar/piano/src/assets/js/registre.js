function showPassword() {
  let password = document.querySelector("#contrasenya");
  let eye = document.querySelector("#hidePasswd");
  let eyeSlash = document.querySelector("#showPasswd");

  if (password.type === "password") {
    password.type = "text";
    eye.style.display = "block";
    eyeSlash.style.display = "none";
  } else {
    password.type = "password";
    eye.style.display = "none";
    eyeSlash.style.display = "block";
  }
}

function showConfirmation() {
  let confirm = document.querySelector("#confirmacio");
  let eye = document.querySelector("#hideConfirm");
  let eyeSlash = document.querySelector("#showConfirm");

  if (confirm.type === "password") {
    confirm.type = "text";
    eye.style.display = "block";
    eyeSlash.style.display = "none";
  } else {
    confirm.type = "password";
    eye.style.display = "none";
    eyeSlash.style.display = "block";
  }
}

document.querySelector("#passwd").addEventListener("click", showPassword);
document.querySelector("#confirm").addEventListener("click", showConfirmation);

document
  .querySelector("input[name=contrasenya]")
  .addEventListener("change", checkPassword);
document
  .querySelector("input[name=confirmacio]")
  .addEventListener("change", checkPassword);

function checkPassword() {
  const password = document.querySelector("input[name=contrasenya]");
  const confirm = document.querySelector("input[name=confirmacio]");
  if (confirm.value === password.value) {
    confirm.setCustomValidity("");
  } else {
    confirm.setCustomValidity("Passwords must match");
  }
}
