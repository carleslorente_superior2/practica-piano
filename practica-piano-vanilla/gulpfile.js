const gulp = require("gulp");
const workboxBuild = require("workbox-build");

gulp.task("service-worker", () => {
  return workboxBuild.generateSW({
    globDirectory: "/",
    globPatterns: ["**/*.{html,json,js,css,ogg,ico,png,jpg,mp4}"],
    swDest: "./service-worker.js",
    runtimeCaching: [
      {
        urlPattern: /\.(?:html|json|js|css|ogg|ico|png|jpg|mp4)$/,
        handler: "CacheFirst",

        options: {
          cacheName: "images",
          expiration: {
            maxEntries: 50,
          },
        },
      },
    ],
  });
});
