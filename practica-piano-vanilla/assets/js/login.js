function showPassword() {
  let password = document.querySelector("#password");
  let eye = document.querySelector("#hide1");
  let eyeSlash = document.querySelector("#hide2");

  if (password.type === "password") {
    password.type = "text";
    eye.style.display = "block";
    eyeSlash.style.display = "none";
  } else {
    password.type = "password";
    eye.style.display = "none";
    eyeSlash.style.display = "block";
  }
}
