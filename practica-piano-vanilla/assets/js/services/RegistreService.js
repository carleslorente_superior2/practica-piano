export async function registre(email, nom, password) {
  const fetchRegistre = await fetch(
    "https://server247.cfgs.esliceu.net:24743/piano/auth/signup",
    {
      method: "POST",
      body: JSON.stringify({
        usuari: {
          email: email,
          nom: nom,
        },
        password: password,
      }),
    }
  );

  return fetchRegistre.json();
}
